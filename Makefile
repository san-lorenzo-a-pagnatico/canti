# Makefile per i libretti dei canti
# Copyright (C) 2017 Davide G. M. Salvetti

SHELL := /bin/bash
DIRS := libretti celebrazioni public
README := README.md

.PHONY: all
all: $(README)
	@$(MAKE) TEXINPUTS=../tex/: libretti

%.md: %.org
	emacs $< --batch --load ox-md -f org-md-export-to-markdown

.PHONY: $(DIRS)
$(DIRS):
	@$(MAKE) TEXINPUTS=../tex/: -C $@

.PHONY: send
send:
	@$(MAKE) -C celebrazioni $@

.PHONY: clean distclean
clean: $(README)
	@for DIR in $(DIRS); do $(MAKE) -C $${DIR} $@; done
	rm -f -- *.{aux,gaux,gtex,log,sxd,sbx,sxc,synctex.gz}

.PHONY: distclean
distclean: clean
	@for DIR in $(DIRS); do $(MAKE) -C $${DIR} $@; done
	rm -f prova.pdf
