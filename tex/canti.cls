\NeedsTeXFormat{LaTeX2e}%
\def\@getcl@ss#1.cls#2\relax{\def\@currentclass{#1}}%
\expandafter\@getcl@ss\@filelist\relax%
\ProvidesClass{\@currentclass}%
[2017/11/04\space v0.2\space Stile per raccolte di canti]%
\RequirePackage{xkeyval}%
\PassOptionsToPackage{unicode}{hyperref}%
\newif\ifcantimobile\cantimobilefalse%
\define@choicekey{\@currentclass.cls}{formato}[\val\nr]{a4,a5,mobile}{%
  \ifcase\nr\relax
  \def\libretto@formato{a4}%
  \PassOptionsToClass{a4paper}{article}%
  \AtEndOfClass{%
    \geometry{ignoreall, includefoot, margin = 1.0cm}%
    \renewcommand{\headrulewidth}{0pt}%
    \fancyhf{}\fancyhead[R]{\raisebox{-1.0cm}[0pt][0pt]{%
        \makebox[0pt]{\hskip1.2cm\rotatebox{-90}{%
            \textsf{\rightmark}}}}}%
    \pagestyle{fancy}%
    \renewcommand{\sectionmark}[1]{\markright{##1}}%
    \songcolumns{2}%
    \indexsongsas{generale}{\thesongnum}%
  }%
  \or
  \def\libretto@formato{a5}%
  \PassOptionsToClass{a5paper}{article}%
  \AtEndOfClass{%
    \geometry{ignoreall, includefoot, margin = 1.0cm}%
    \renewcommand{\headrulewidth}{0pt}%
    \fancyhf{}\fancyhead[R]{\raisebox{-1.0cm}[0pt][0pt]{%
        \makebox[0pt]{\hskip1.2cm\rotatebox{-90}{%
            \textsf{\rightmark}}}}}%
    \pagestyle{fancy}%
    \renewcommand{\sectionmark}[1]{\markright{##1}}%
    \songcolumns{2}%
    \indexsongsas{generale}{\thesongnum}%
  }%
  \or
  \cantimobiletrue%
  \def\libretto@formato{mobile}%
  \AtEndOfClass{%56 100
    \geometry{paperwidth = 112mm, paperheight = 199mm,
      ignoreall, includefoot, margin = 2mm, bottom = 7mm}%
    \songcolumns{0}\raggedbottom%
    \renewcommand{\colbotglue}{0pt plus \textheight minus 0pt}%
    \renewcommand{\spenalty}{-10000}%
    \indexsongsas{generale}{\thesongnum}%
    \pagestyle{empty}%
  }%
  \fi
}
\define@boolkey{\@currentclass.cls}[canti]{indici}[false]{
  \ifcantiindici\relax\else
  \PassOptionsToPackage{noindexes}{songs}\relax
  \fi
}
\define@choicekey{\@currentclass.cls}{tipo}[\val\nr]{accordi,testi,prova}{%
  \ifcase\nr\relax
  \def\libretto@tipo{accordi}%
  \PassOptionsToPackage{chorded, nomeasures}{songs}%
  \AtEndOfClass{%
  \renewcommand{\lyricfont}{\large}}%
  \or
  \def\libretto@tipo{testi}%
  \PassOptionsToPackage{lyric}{songs}%
  \or
  \def\libretto@tipo{prova}%
  \def\libretto@formato{a4}%
  \PassOptionsToClass{a4paper}{article}%
  \PassOptionsToPackage{chorded}{songs}%
  \AtEndOfClass{%
    \geometry{ignoreall, includefoot, margin = 1.0cm}%
    \songcolumns{1}\pagestyle{empty}}%
  \fi
}
\DeclareOptionX{minore}{\def\@minore{#1}}%
\DeclareOptionX*{\PassOptionsToClass{\CurrentOption}{article}}%
\ExecuteOptionsX{tipo=prova, minore={\textendash}}%
\ProcessOptionsX\relax%
\LoadClass{article}\RequirePackage{xcolor}%
\RequirePackage{luatextra}% comprende fontspec luacode fixltx2e
\RequirePackage{bookmark, calc, fancyhdr, geometry, graphicx, ifthen,
  microtype, polyglossia, relsize, songs}%
\RequirePackage{gregoriotex, ragged2e}%
\setmainfont[Ligatures=TeX]{TeX Gyre Pagella}%
\setsansfont[Ligatures = TeX, Scale = MatchLowercase]{TeX Gyre Heros}%
\setmonofont[Scale = MatchLowercase]{Inconsolata}%
\setmainlanguage{italian}%
\def\bassfont{\futurelet\next\@bassverify}%
\def\nextchord{\large\space}% restore default chord font
\def\@basstrue{\hspace{-0.2em}/\hspace{-0.05em}\scriptsize}%
\def\@bassfalse{/}%
\def\@bassverify{%
  \ifcat\next f
  \@basstrue
  \else
  \@bassfalse
  \fi
}%
\renewcommand{\chordlocals}{\catcode`/\active\catcode`-\active\catcode` \active}%
{\chordlocals\global\let/\bassfont\global\let-\@minore\global\let \nextchord}%
\newindex{generale}{libretto+\libretto@formato-\libretto@tipo-generale}%
\newindex{messa}{libretto+\libretto@formato-\libretto@tipo-messa}%
\newindex{avvento}{libretto+\libretto@formato-\libretto@tipo-avvento}%
\newindex{natale}{libretto+\libretto@formato-\libretto@tipo-natale}%
\newindex{quaresima}{libretto+\libretto@formato-\libretto@tipo-quaresima}%
\newindex{pasqua}{libretto+\libretto@formato-\libretto@tipo-pasqua}%
\newindex{spirito}{libretto+\libretto@formato-\libretto@tipo-spirito}%
\newindex{maria}{libretto+\libretto@formato-\libretto@tipo-maria}%
\newindex{santi}{libretto+\libretto@formato-\libretto@tipo-santi}%
\newindex{salmi}{libretto+\libretto@formato-\libretto@tipo-salmi}%
\newindex{ordinario}{libretto+\libretto@formato-\libretto@tipo-ordinario}%
\newindex{esequie}{libretto+\libretto@formato-\libretto@tipo-esequie}%
\newindex{eucaristia}{libretto+\libretto@formato-\libretto@tipo-eucaristia}%
\newindex{altri}{libretto+\libretto@formato-\libretto@tipo-altri}%
\newindex{ingresso}{libretto+\libretto@formato-\libretto@tipo-ingresso}%
\newindex{offertorio}{libretto+\libretto@formato-\libretto@tipo-offertorio}%
\newindex{congedo}{libretto+\libretto@formato-\libretto@tipo-congedo}%
\titleprefixword{}%
\newchords{rit}\newchords{m1}\newchords{m2}\newchords{m3}%
\renewcommand{\idxtitlefont}{\sffamily\mdseries\upshape}%
\renewcommand{\idxlyricfont}{\sffamily\mdseries\itshape}%
\renewcommand{\idxrefsfont}{\sffamily\mdseries\upshape}
\notenamesin{A}{B}{C}{D}{E}{F}{G}%
\notenamesout{LA}{SI}{DO}{RE}{MI}{FA}{SOL}%
\newcommand*{\indice}{\showindex{Indice dei canti}{generale}}%
\newcommand*{\indici}{%
  \showindex{Ordinario della Messa}{messa}%
  \showindex{Tempo di Avvento}{avvento}%
  \showindex{Tempo di Natale}{natale}%
  \showindex{Tempo di Quaresima}{quaresima}%
  \showindex{Settimana Santa e Tempo di Pasqua}{pasqua}%
  \showindex{Pentecoste e invocazioni allo Spirito Santo}{spirito}%
  \showindex{Festività mariane}{maria}%
  \showindex{Feste dei santi}{santi}%
  \showindex{Salmi}{salmi}%
  \showindex{Tempo ordinario}{ordinario}%
  \showindex{Esequie}{esequie}%
  \showindex{Altri canti}{altri}%
  \showindex{Ingresso}{ingresso}%
  \showindex{Offertorio}{offertorio}%
  \showindex{Eucaristia}{eucaristia}%
  \showindex{Congedo}{congedo}%
}%
\newcommand*{\inserisci}[2][]{%
  \ifthenelse{\equal{#1}{}}{}{%
    \global\edef\canti@indexlist{\SB@indexlist}%
    \global\edef\SB@indexlist{\canti@indexlist,#1}}%
  \input{../canti/#2}%
  \ifthenelse{\equal{#1}{}}{}{%
    \gdef\SB@indexlist{\canti@indexlist}}%
}%
\renewcommand{\thesongnum}{\Alph{section}\arabic{songnum}}%
\newcommand*{\momento}[1]{%
  \intersong\sffamily\bfseries\large\noindent
  #1\par\nointerlineskip\endintersong}%
\newenvironment*{celebrazione}[1]{%\nosongnumbers%
  \begin{songs}{}%
    \begin{intersong*}%
      \noindent
      \ifcantimobile
      \vspace*{\fill}%
      \begin{center}
        Canti per la celebrazione\\\vspace{3\bigskipamount}
        {\small\makebox[\linewidth]{\hfill #1\hfill}}
      \end{center}%
      \vspace*{\fill}%
      \else
      \LARGE\colorbox{SongbookShade}{\makebox[\linewidth]{#1}}%
      \fi
    \end{intersong*}%
    \ifcantimobile\else\vspace*{\medskipamount}\nopagebreak[4]\fi%
  }{\end{songs}}
\newenvironment*{prova}{\songcolumns{1}%
  \begin{songs}{}
    \begin{intersong*}%
      \noindent\LARGE\colorbox{SongbookShade}{\makebox[\linewidth]{%
          Prova tipografica}}%
    \end{intersong*}%
    \vspace*{\medskipamount}\nopagebreak[4]\relax%
}{\end{songs}}
\def\<#1>{\underline{#1}}%
\endinput

% local variables:
% mode: latex
% ispell-local-dictionary: "italiano"
% end:
