

# Descrizione generale

Questo progetto ha lo scopo di generare i libretti dei canti della parrocchia,
sia in versione solo testo per l'assemblea, sia in versione testo con accordi
per l'accompagnamento musicale.

In aggiunta ai libretti, è prevista anche la possibilità di stampare fogli con
i canti per le singole celebrazioni, sia in versione solo testo, sia in
versione testo con accordi.

Le ultime versioni dei libretti sono disponibili in rete all'indirizzo
pubblico <https://san-lorenzo-a-pagnatico.gitlab.io/canti/>, link breve
<http://tiny.cc/slap-canti> e sono rigenerate automaticamente per ogni commit
sulla master branch.


# Compilare i libretti

Per compilare i libretti:

    make


# Aggiungere un canto

1.  Scrivere il canto in un nuovo file nella directory principale seguendo il
    modello del file [prova.tex](prova.tex).  Sostituire opportunamente solo le parti tra
    `\beginsong` ed `\endsong`, lasciando inalterate le prime e le ultime
    righe, che devono rimanere le stesse del file [prova.tex](prova.tex).
2.  Compilare il nuovo file per verificarne la correttezza.
3.  Una volta soddisfatti del lavoro:
    -   rimuovere le righe che precedono `\beginsong` ed `\endsong`;
    -   spostare il nuovo file nella directory [canti/](canti/);
    -   aggiungere il canto nella sezione opportuna del file [libretti/canti.tex](libretti/canti.tex)
        usando il comando `\inserisci`:
        
            \inserisci{<indici>}{nuovo-canto}
        
        dove `<indici>` è la lista di indici tematici in cui il canto deve
        comparire.


## Registri per gli accordi

In aggiunta al registro di default la classe [tex/canti.cls](tex/canti.cls) definisce i
registri per le sequenze di accordi `rit`, `m1`, `m2`, `m3`, che possono
essere usati in caso di bisogno con `\memorize[<reg>]` e `\replay[<reg>]`.


## Notazione degli accordi

Per ragioni di uniformità, gli accordi devono essere scritti in notazione
alfabetica (es.: *Sol7* => *G7*) e gli accordi minori devono essere indicati
con il segno *-* (es.: *Lam* => *A-*).  Questo garantisce la possibilità di
scegliere programmaticamente se gli accordi in uscita devono essere in
notazione italiana oppure alfabetica senza necessità di modificare i sorgenti
`.tex`.

Le alterazioni devono essere indicate con il simbolo `#` per il diesis e `&`
per il bemolle.  Questo, insieme all'accorgimento di cui sopra, garantisce la
possibilità di trasporre automaticamente gli accordi in un'altra qualunque
tonalità senza necessità di modificare i sorgenti.


## Posizione degli accordi

Gli accordi devono essere posizionati subito prima della sillaba sulla quale
devono essere suonati, che spesso (ma non sempre) è la sillaba tonica della
parola.  Ad esempio, con riferimento al primo verso della [Canzone di San
Damiano](canti/canzone-di-san-damiano.tex):

    \[D-]Ogni \[C]uomo \[D-]sempli\[C]ce, \[D-]porta in \[C]cuore un  \[D-]so\[C]gno,

Se l'accordo deve suonare *prima* della sillaba, è possibile racchiuderlo tra
graffe (vedere il paragrafo *Chords* della documentazione del pacchetto
*Songs*; nella documentazione della versione 3.0 del pacchetto, del 5 giugno
2017, il paragrafo è il numero 5.3, [disponibile anche online](http://songs.sourceforge.net/songsdoc/songs.html#sec5.3).  Ad esempio, con
riferimento all'inizio del secondo verso di [Amatevi fratelli](canti/amatevi-fratelli.tex):

    {\[E]} come \[A-]io ho a\[D7]mato \[G7]voi!


## Spaziatura tra le parole e tra le sillabe

È importante tener presente che la spaziatura tra le parole e tra le sillabe
delle parole viene determinata automaticamente dall'algoritmo tipografico di
LaTeX, così come modificato dal pacchetto *Songs*.  Pertanto è inutile e anzi
dannoso cercare di spaziare *a mano* aggiungendo spazi nel sorgente che
vengono ignorati o male interpretati.  Il già citato paragrafo 5.3 mostra come
non ci sia bisogno di separare manualmente le sillabe per far posto ad accordi
*lunghi* (vedere la parte iniziale del paragrafo).


## *Seconde voci*, *controcanti*, *ripetizioni*

In caso di seconde voci, la spaziatura e la presentazione tipografica
opportuna (eventualmente modificabile da programma) si possono ottenere usando
il comando `\echo` (vedere il [paragrafo 5.6](http://songs.sourceforge.net/songsdoc/songs.html#sec5.6) della documentazione citata).  Ad
esempio con riferimento alla prima strofa di [Madre della speranza](canti/madre-della-speranza.tex):

    \beginverse
    \[B-]Docile \[E-]serva del \[B-]Padre \echo{Maria}
    \[A]piena di Spirito \[B-]Santo \echo{Maria}
    \[G]umile \[A]Vergine, \[D]Madre del \[E-]Figlio di \[B- E-]{Di}\[F#]o!
    \[B-]Tu sei la \[E-]Piena di \[B-]Grazia \echo{tutta bella sei}
    \[A]scelta fra tutte le \[B-]donne, \echo{non c'è ombra in te}
    \[G]Madre di \[A]Miseri\[D]cordia, \[E-]Porta del \[B- E-]{cie}\[F#]lo. \[A7]
    \endverse

Per indicare la ripetizione di un versetto si possono usare i comandi `\lrep`
`\rrep` e `\rep{<n>}` (vedere sempre il [paragrafo 5.6](http://songs.sourceforge.net/songsdoc/songs.html#sec5.6)).  Ad esempio, con
riferimento al [Santo (Gen Rosso)](canti/santo-gen-rosso.tex):

    \beginverse*
    \lrep~O|\[G]san\[D]na, o|\[G]san\[D]na,
    o|\[E-]sanna nel\[D7]l'alto dei |\[G]cieli.|~\rrep~\rep{2}
    \endverse

Nota: in italiano è preferire inserire gli spazi che si ottengono scrivendo
«\lrep~» e simili anziché «\lrep», proprio come nell'esempio precedente.


# Preparare la prossima celebrazione

1.  Preparare un file nella directory [celebrazioni](celebrazioni/) con l'elenco dei canti
    scelti sul modello di uno già esistente, per esempio [20171105-a-to-31.tex](celebrazioni/20171105-a-to-31.tex)
    (è consigliabile usare un nome della forma `YYYYMMDD-descrizione.tex`).
2.  Eseguire `make celebrazioni`.
3.  Consultare i file di nome `YYYYMMDD-descrizione+*.pdf`.


# Convertire una strofa con accordi annotati in formato testo

Usando il filtro [bin/text2songs.pl](bin/text2songs.pl) è possibile covertire strofe annotate con
accordi in formato testo, come ad esempio [doc/strofa-esempio.txt](doc/strofa-esempio.txt), in strofe
con accordi annotati in formato *Songs*:

    bin/text2songs.pl < doc/strofa-esempio.txt | tee doc/strofa-esempio.tex

N.B.: Il filtro si aspetta che la strofa in ingresso abbia gli accordi sulle
righe dispari ed i versi corrispondenti sulla riga pari immediatamente
successiva, nella quale l'accordo viene inserito nella posizione
corrispondente per numero di caratteri dall'inizio della riga.

Il filtro si aspetta accordi in ingresso scritti con i nomi delle note in
notazione italiana oppure alfabetica e restituisce in uscita accordi con i
nomi scritti in notazione alfabetica.


# Tutto il resto

Scrivere: salve.linux.it (sostituire il carattere giusto al posto del primo
punto).

