#! /usr/bin/perl
# Copyright (C) 2017 Davide G. M. Salvetti

=head1 NAME

=encoding utf8

text2songs - filtro per convertire strofe con accordi da testo a songs

=head1 SYNOPSIS

  text2songs < I<input.txt> > I<ouput.tex>

=head1 DESCRIPTION

Riceve sullo standard input una strofa con accordi annotati in formato
testo e la invia allo standard output con accordi annotati nel formato del
pacchetto LaTeX B<songs>.

=cut

use strict;
use warnings;
use utf8;
use open qw(:std :utf8);
use feature qw(fc say);
use YAML::XS;

my $debug = 0;
my $help = {usage => "Uso: $0 < input.txt > output.tex"};
unless (@ARGV == 0) {say $help->{usage}; exit 1;}

my %latin2alpha = (
  do  => 'C',
  re  => 'D',
  mi  => 'E',
  fa  => 'F',
  sol => 'G',
  la  => 'A',
  si  => 'B',
);
my $notenames = join '|', keys %latin2alpha;
my $line = 0;
my @chords;
while (<>) {
  $line++;
  chomp;
  if ($line % 2) {
    say "$line: chords: $_" if $debug;
    push @chords, ({chord => $1, at => $-[1]}) while /(\S+)/g;
    say Dump (@chords) if $debug;
    $_->{chord} =~ s/($notenames)/$latin2alpha{lc $1}/ig for (@chords);
    say Dump (@chords) if $debug;
  } else {
    say "$line: text..: $_" if $debug;
    my $offset = 0;
    for my $c (@chords) {
      say "offset: $offset" if $debug;
      my $cs = sprintf ('\[%s]', $c->{chord});
      substr ($_, $c->{at} + $offset, 0) = $cs;
      $offset += length ($cs);
    }
    say;
    @chords = ();
  }
}

exit 0;
__END__

=head1 SEE ALSO

L<songs>

=head1 AUTHOR

Davide G. M. Salvetti, E<lt>salve@linux.it<gt>.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2017 Davide G. M. Salvetti.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see L<http://www.gnu.org/licenses/>.

=cut

# local variables:
# mode: perl
# coding: utf-8
# ispell-local-dictionary: "american"
# ispell-check-comments: exclusive
# end:
